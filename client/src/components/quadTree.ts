import { ColliderComponent } from "../components/colliderComponent";
import { Rectangle } from "./rectangle";

export interface IQuadTreeDesc {
    width: number;
    height: number;
}

interface IQuadTreeDescAlt {
    cornerID: number;
    quadTree: QuadTree;
}

function isQuadTreeDesc(arg: IQuadTreeDesc | IQuadTreeDescAlt): arg is IQuadTreeDesc {
    return (arg as IQuadTreeDesc).width !== undefined;
}

// ## Classe *QuadTree*
// Classe pour représenter un quad tree.
export class QuadTree {
    public quad!: Rectangle;
    public depth: number;
    public id: number;
    public children: QuadTree[] = [];

    // ### Constructeur de la classe *QuadTree*
    constructor(descr: IQuadTreeDesc | IQuadTreeDescAlt) {
        if (isQuadTreeDesc(descr)) {
            this.quad = new Rectangle({
                xMin : 0,
                xMax : descr.width,
                yMin : 0,
                yMax : descr.height,
            });
            this.depth = 0;
            this.id = 0;
            // First Call : Recursive Generation function
            this.FillChildren();
        } else {
            if(descr.cornerID == 1)
            {
                this.TopLeftQuadTree(descr);
            }
            else if(descr.cornerID == 2)
            {
                this.TopRightQuadTree(descr);
            }
            else if(descr.cornerID == 3)
            {
                this.BottomLeftQuadTree(descr);
            }
            else if(descr.cornerID == 4)
            {
                this.BottomRightQuadTree(descr);
            }
            this.depth = descr.quadTree.depth + 1;
            this.id = 4 * descr.quadTree.depth * descr.quadTree.id + descr.cornerID;
        }
    }

    TopLeftQuadTree(descr: IQuadTreeDescAlt)
    {
        const width: number = descr.quadTree.quad.xMax - descr.quadTree.quad.xMin;
        const height: number = descr.quadTree.quad.yMax - descr.quadTree.quad.yMin;
        this.quad = new Rectangle({
            xMin : descr.quadTree.quad.xMin,
            xMax : descr.quadTree.quad.xMin + width / 2,
            yMin : descr.quadTree.quad.yMax - height / 2,
            yMax : descr.quadTree.quad.yMax,
        });
    }

    TopRightQuadTree(descr: IQuadTreeDescAlt)
    {
        const width: number = descr.quadTree.quad.xMax - descr.quadTree.quad.xMin;
        const height: number = descr.quadTree.quad.yMax - descr.quadTree.quad.yMin;
        this.quad = new Rectangle({
            xMin : descr.quadTree.quad.xMax - width / 2,
            xMax : descr.quadTree.quad.xMax,
            yMin : descr.quadTree.quad.yMax - height / 2,
            yMax : descr.quadTree.quad.yMax,
        });
    }

    BottomLeftQuadTree(descr: IQuadTreeDescAlt)
    {
        const width: number = descr.quadTree.quad.xMax - descr.quadTree.quad.xMin;
        const height: number = descr.quadTree.quad.yMax - descr.quadTree.quad.yMin;
        this.quad = new Rectangle({
            xMin : descr.quadTree.quad.xMin,
            xMax : descr.quadTree.quad.xMin + width / 2,
            yMin : descr.quadTree.quad.yMin,
            yMax : descr.quadTree.quad.yMin + height / 2,
        });
    }

    BottomRightQuadTree(descr: IQuadTreeDescAlt)
    {
        const width: number = descr.quadTree.quad.xMax - descr.quadTree.quad.xMin;
        const height: number = descr.quadTree.quad.yMax - descr.quadTree.quad.yMin;
        this.quad = new Rectangle({
            xMin : descr.quadTree.quad.xMax - width / 2,
            xMax : descr.quadTree.quad.xMax,
            yMin : descr.quadTree.quad.yMin,
            yMax : descr.quadTree.quad.yMin + height / 2,
        });
    }

    // Method *FillChildren*
    // Fill quad tree children with 4 more quadtrees : create subdivisions
    FillChildren()
    {
        if(this.depth > 2) return; // Max depth

        const topLeft : QuadTree = new QuadTree({
            cornerID: 1,
            quadTree: this,
          });
        this.children.push(topLeft);
        topLeft.FillChildren();

        const topRight : QuadTree = new QuadTree({
            cornerID: 2,
            quadTree: this,
          });
        this.children.push(topRight);
        topRight.FillChildren();

        const bottomLeft : QuadTree = new QuadTree({
            cornerID: 3,
            quadTree: this,
          });
        this.children.push(bottomLeft);
        bottomLeft.FillChildren();

        const bottomRight : QuadTree = new QuadTree({
            cornerID: 4,
            quadTree: this,
          });
        this.children.push(bottomRight);
        bottomRight.FillChildren();
    }

    // Method *IsSharingSubdivision*
    // Does c1 sharing a subdivision with c2 ?
    IsSharingSubdivision(c1 : ColliderComponent, c2 : ColliderComponent)
    {
        const r1 = c1.area;
        const r2 = c2.area;

        const r1IDs = this.GetIDs(r1);
        const r2IDs = this.GetIDs(r2);

        var commonElements = r1IDs.filter(function (e) {
            return r2IDs.indexOf(e) > -1;
        });
        const length: number = commonElements.length;
        return length > 0;
    }

    // Method *GetIDs*
    // Return a index array according to subdivision belonging of the rectangle r
    GetIDs(r: Rectangle): number[]
    {
        var quadTreeID: number[] = []; // result
        for (let index = 0; index < this.children.length; index++) {
            const element = this.children[index];
            if(element.children.length > 0)
            {
                // Recursive call for children
                quadTreeID = quadTreeID.concat(element.GetIDs(r));
            }
            else
            {
                // No more children, we can get quad tree ids
                if(r.intersectsWith(element.quad))
                {
                    // r in quad area
                    quadTreeID.push(element.id)
                }
            }
        }
        return quadTreeID;
    }
}
