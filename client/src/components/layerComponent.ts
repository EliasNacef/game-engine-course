import * as GraphicsAPI from "../graphicsAPI";
import { IEntity } from "../entity";
import { IDisplayComponent } from "../systems/displaySystem";
import { Component } from "./component";
import { SpriteComponent } from "./spriteComponent";
import { TextureComponent } from "./textureComponent";
let GL: WebGLRenderingContext;

// # Classe *LayerComponent*
// Ce composant représente un ensemble de sprites qui
// doivent normalement être considérées comme étant sur un
// même plan.
export class LayerComponent extends Component<object> implements IDisplayComponent {
  private vertices!: Float32Array;
  private vertexBuffer!: WebGLBuffer;
  private indices!: Uint16Array;
  private indexBuffer!: WebGLBuffer;

  // ## Méthode *display*
  // La méthode *display* est appelée une fois par itération
  // de la boucle de jeu.
  public display(dT: number) {
    const layerSprites = this.listSprites();
    if (layerSprites.length === 0) {
      return;
    }

    // initialization
    const spriteSheet = layerSprites[0].spriteSheet; // the sprite sheet to display
    const vertexNumber = 4; // 4 vertices per sprite : square
    const indiceNumber = 6; // 6 indices per sprite : 3 triangles
    this.vertices = new Float32Array(layerSprites.length * vertexNumber * TextureComponent.vertexSize);
    this.indices = new Uint16Array(layerSprites.length * indiceNumber);

    // set vertices and indices
    for (let i = 0; i < layerSprites.length; ++i) {
      const dataIndex = vertexNumber * i;
      this.vertices.set(layerSprites[i].vertices, TextureComponent.vertexSize * dataIndex);
      this.indices.set(new Uint16Array([dataIndex, dataIndex + 1, dataIndex + 2, dataIndex + 2, dataIndex + 3, dataIndex]), indiceNumber * i);
    }

    // get webgl
    GL = GraphicsAPI.context;
    // vertex buffering
    this.vertexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, this.vertices, GL.DYNAMIC_DRAW);
    
    // indice buffering
    this.indexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, this.indices, GL.DYNAMIC_DRAW);

    // do the draw call : each sprite above will be displayed with a single call
    spriteSheet.bind();
    GL.drawElements(GL.TRIANGLES, indiceNumber * layerSprites.length, GL.UNSIGNED_SHORT, 0);
    spriteSheet.unbind();
  }

  // ## Fonction *listSprites*
  // Cette fonction retourne une liste comportant l'ensemble
  // des sprites de l'objet courant et de ses enfants.
  private listSprites() {
    const sprites: SpriteComponent[] = [];

    const queue: IEntity[] = [this.owner];
    while (queue.length > 0) {
      const node = queue.shift() as IEntity;
      for (const child of node.children) {
        if (child.active) {
          queue.push(child);
        }
      }

      for (const comp of node.components) {
        if (comp instanceof SpriteComponent && comp.enabled) {
          sprites.push(comp);
        }
      }
    }

    return sprites;
  }
}
