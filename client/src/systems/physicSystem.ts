import { ColliderComponent } from "../components/colliderComponent";
import { QuadTree } from "../components/quadTree";
import { Scene } from "../scene";
import { ISystem } from "./system";
import * as GraphicsAPI from "../graphicsAPI";


// # Classe *PhysicSystem*
// Représente le système permettant de détecter les collisions
export class PhysicSystem implements ISystem {

  initialized:boolean = false;
  quadTree!: QuadTree;

  // Méthode *iterate*
  // Appelée à chaque tour de la boucle de jeu
  public iterate(dT: number) {
    if(!this.initialized)
    {
      const sceneHeight: number = GraphicsAPI.canvas.height;
      const sceneWidth: number = GraphicsAPI.canvas.width;
      this.quadTree = new QuadTree({
        width: sceneWidth,
        height: sceneHeight
      });
      this.initialized = true;
    }

    const colliders: ColliderComponent[] = [];
    for (const e of Scene.current.entities()) {
      for (const comp of e.components) {
        if (comp instanceof ColliderComponent && comp.enabled) {
          colliders.push(comp);
        }
      }
    }

    // Quad tree creation
    const sceneHeight : number = GraphicsAPI.canvas.height
    const sceneWidth : number = GraphicsAPI.canvas.width
    const quadTree : QuadTree = new QuadTree({
      width: sceneWidth,
      height: sceneHeight
    });

    const collisions: Array<[ColliderComponent, ColliderComponent]> = [];

    for (let i = 0; i < colliders.length; i++) {
      const c1 = colliders[i];
      if (!c1.enabled || !c1.owner.active) {
        continue;
      }

      for (let j = i + 1; j < colliders.length; j++) 
      {
        const c2 = colliders[j];

        if (!c2.enabled || !c2.owner.active) {
          continue;
        }

        // Flag checking
        var relevance = c1.flag & c2.mask;
        if(relevance != 0)
        {
          // if flag and are compatibles : collision is possible
          if(quadTree.IsSharingSubdivision(c1, c2))
          {
            // share at least one subdivision in quadtrees
            if (c1.area.intersectsWith(c2.area))  
            {
              // rectangle intersection 
              collisions.push([c1, c2]);
            }
          }
        }
      }
    }

    for (const [c1, c2] of collisions) {
        if (c1.handler) {
          c1.handler.onCollision(c2);
        }
        if (c2.handler) {
          c2.handler.onCollision(c1);
        }
    }
  }
}
