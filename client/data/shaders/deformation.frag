precision mediump float;

/* Rendu du jeu */
uniform sampler2D uSampler;

/* Texture de déformation en rouge et vert */
uniform sampler2D uDeformation;

/* Texture pour contrôler l'intensité de la déformation */
uniform sampler2D uIntensity;

/* Interval de temps multiplié par la vitesse depuis l'activation du composant */
uniform float uTime;

/* Échelle de la déformation */
uniform float uScale;

/* Coordonnées UV du fragment */
varying vec2 vTextureCoord;

void main(void) {
    vec2 intensityTextureCoord = vec2(uTime, 0.5);
    vec4 intensityTexture = texture2D(uIntensity, intensityTextureCoord) * uScale;
    float xOffset = cos(uTime);
    float yOffset = sin(uTime);
    vec2 deformationTextureCoord = vTextureCoord + vec2(xOffset, yOffset);
    vec4 deformationTexture = intensityTexture * (texture2D(uDeformation, deformationTextureCoord) - 0.5);
    gl_FragColor = texture2D(uSampler, vTextureCoord + deformationTexture.xy);
    gl_FragColor.gb *= 0.5;
}
